@ECHO off
REM This is free and unencumbered software released into the public domain.

REM Anyone is free to copy, modify, publish, use, compile, sell, or
REM distribute this software, either in source code form or as a compiled
REM binary, for any purpose, commercial or non-commercial, and by any
REM means.

REM In jurisdictions that recognize copyright laws, the author or authors
REM of this software dedicate any and all copyright interest in the
REM software to the public domain. We make this dedication for the benefit
REM of the public at large and to the detriment of our heirs and
REM successors. We intend this dedication to be an overt act of
REM relinquishment in perpetuity of all present and future rights to this
REM software under copyright law.

REM THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
REM EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
REM MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
REM IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
REM OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
REM ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
REM OTHER DEALINGS IN THE SOFTWARE.

REM For more information, please refer to <http://unlicense.org>

PATH=%CD%;%PATH%
REM Install 7-zip in %TEMP%\7ZIP
CURL --output %TEMP%\7ZIP.ZIP https://www.7-zip.org/a/7za920.zip
POWERSHELL
Expand-Archive %TEMP%\7ZIP.ZIP %TEMP%\7ZIP\
EXIT
DEL %TEMP%\7ZIP.ZIP
REM Download unbound and extract to %TEMP%\UNBOUND\
CURL --output %TEMP%\UNBOUND.EXE https://nlnetlabs.nl/downloads/unbound/unbound_setup_1.8.3.exe
%TEMP%\7ZIP\7za.exe x %TEMP%\UNBOUND.EXE -o %TEMP%\UNBOUND\
DEL %TEMP%\UNBOUND.EXE
REM Copy unbound files to Program Files
MKDIR COPY UNBOUND %PROGRAMFILES%\Unbound
CD %PROGRAMFILES%\Unbound
REM Install unbound service
unbound-anchor.exe
unbound-service-install.exe
unbound-service-install.exe start
ECHO Done!